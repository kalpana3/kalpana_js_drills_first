let func = require('./index.cjs');
let len = func.length;
//console.log(len);

function problem2(func, len){
    return (`Last Car is a ${func[len-1].car_make} ${func[len-1].car_model} ${func[len-1].car_year}`);
}


let s = problem2(func, len);

module.exports = s ;