let imp = require('./index.cjs')


function sorting(imp){
    imp.sort((a,b) => {
        if(a.car_model > b.car_model){
            return 1;
        } else if(a.car_model < b.car_model){
            return -1;
        }else {
            return 0;
        }
    });
    let obj = [];
    for(var i=0; i<imp.length; i++){
      obj[i] = imp[i].car_model;
    }
    return obj;
}

let s = sorting(imp);

module.exports = s ;

