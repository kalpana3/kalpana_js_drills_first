let inv = require('./index.cjs');

function years(inv){
    let array = [];
    for(var i=0; i < inv.length; i++){
        array[i] = inv[i].car_year;
    }
    return array;
}

let s = years(inv);

module.exports = s ;
